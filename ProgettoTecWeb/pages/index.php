<?php
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true	) {

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Studenti Online - Università di Bologna</title>



    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link rel="stylesheet" href="../dist/css/font-awesome-animation.min.css">

    <!-- Morris Charts CSS -->
  <!--  <link href="../vendor/morrisjs/morris.css" rel="stylesheet">-->

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!--  <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- jQuery -->


    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="wrapper">

        <div class="testata">
		<div id="leftColumnCell">
			<a href="http://www.unibo.it">
			<img src="https://starc.unibo.it/images/logo_unibo.gif" alt="Logo dell'Università di Bologna - link alla home page del Portale" title="Logo dell'Università di Bologna - link alla home page del Portale">
			</a>
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Alma Mater Studiorum | Università di Bologna</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope faa-shake animated fa-fw" style="color: #d9534f"></i> <i class="fa fa-caret-down" style="color: #d9534f"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>Alma Mater Studiorum – Newsletter </strong>
                                    <span class="pull-right text-muted">
                                        <em>Ieri</em>
                                    </span>
                                </div>
                                <div>
                                  Calcola l’importo delle tasse per l’a.a. 2017/18
                                    Per l'a.a. 2017/18 l’Università di Bologna ha adottato un nuovo sistema di calcolo ...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Leggi tutti i messaggi</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>

                    <!-- /.dropdown-messages -->
                </li>

                <!-- /.dropdown -->

                <li class="dropdown" style="cursor: pointer; cursor: hand;">
                    <a class="dropdown-toggle" data-toggle="dropdown" onclick = "show1()">
                        <i class="fa fa-calendar fa-fw" style="color: #d9534f"></i> <i class="fa fa-caret-down" style="color: #d9534f"></i>
                    </a>
                    <ul id="c1" class="dropdown-menu dropdown-tasks">

                       <li id="calendario1">

  					        <?php include("calendar_it.php"); ?>

					   </li>
                    </ul>

                    <!-- /.dropdown-tasks -->
					<script>
						function show1(){

							if(document.getElementById("c1").style.display == "none"){
								document.getElementById("c1").style.display = "block";
							} else{
								document.getElementById("c1").style.display = "none";
							}
						}
					</script>
        </li>

                <script  type="text/javascript">

                function cancellaNotifica1(str) {
                      console.log(str);
                      var xmlhttp = new XMLHttpRequest();
                      xmlhttp.onreadystatechange = function() {
                          if (this.readyState == 4 && this.status == 200) {
                          }
                      };
                      xmlhttp.open("GET", "function.php?q=" + str, true);
                      xmlhttp.send();
                }
                var counter;
                var temp=0;
                $.getJSON('../json/js/arrayNotifiche.json', function(data1) {
                  counter = data1.length;
                  for($k=0; $k<data1.length; $k++){

                      if(data1[$k]!=null)
                        temp++;

                  }
                  if(temp > 0 ){
                //  <i class="fa fa-bell faa-ring animated fa-fw" style="color: #d9534f"></i>
                  var campanella = document.createElement("i");
                  campanella.setAttribute('class',"fa fa-bell faa-ring animated fa-fw");
                  campanella.setAttribute('style',"color: #d9534f");
                  document.getElementById("notificaUP").appendChild(campanella);
                } else{
                  var campanella = document.createElement("i");
                  campanella.setAttribute('class',"fa fa-bell fa-fw");
                  campanella.setAttribute('style',"color: #d9534f");
                  document.getElementById("notificaUP").appendChild(campanella);

                }
                });

                function carica1(){
                      $.getJSON('../json/js/arrayNotifiche.json', function(data) {

                      for(i=0; i<counter; i++){
                        if(data[i] != null){
                          var nodeRiquadro = document.createElement("DIV");
                          nodeRiquadro.setAttribute('class',"alert alert-danger alert-dismissible");
                          nodeRiquadro.setAttribute('role',"alert");
                          nodeRiquadro.setAttribute('id',"btn1");

                          var closeButtonn = document.createElement("BUTTON");
                          closeButtonn.setAttribute('type',"button");
                          closeButtonn.setAttribute('class',"close");
                          closeButtonn.setAttribute('data-dismiss',"alert");
                          closeButtonn.setAttribute('id',"btn_close");
                         closeButtonn.setAttribute('onClick',"cancellaNotifica1("+i+")");

                          /*closeButtonn.addEventListener("onClick", function(){
                              cancellaNotifica1(i);
                          });*/

                          var spanX = document.createElement("SPAN");
                          var spanText = document.createTextNode("x");
                          spanX.appendChild(spanText);


                          var nodeA = document.createElement("A");
                          nodeA.setAttribute('href',"situazione_tasse.php");
                          nodeA.setAttribute('style',"padding-left: 0%; margin-left: 0%");
                          nodeA.setAttribute('id',"nodoA");

                          var strongA = document.createElement("STRONG");
                          strongA=document.createTextNode("NOTIFICA");
                          nodeA.appendChild(strongA);

                          var node = document.createElement("DIV");

                          var textnode =  document.createTextNode(data[i].name+ " " + data[i].species);
                          node.appendChild(textnode);

                          document.getElementById("demo").insertAdjacentElement('afterend',nodeRiquadro);
                          document.getElementById("btn1").appendChild(nodeA);
                          document.getElementById("btn1").appendChild(closeButtonn);
                          document.getElementById("btn_close").appendChild(spanX);
                          document.getElementById("btn1").appendChild(node);
                        }
                      }

                        counter=0;
                        });
                        //fa fa-bell fa-fw

                  }

                </script>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" style="cursor: pointer; cursor: hand;"onclick="carica1()" id="notificaUP">
                    <i class="fa fa-caret-down" style="color: #d9534f"></i>
                    </a>
                    <ul class = "dropdown-menu dropdown-alerts-">
                        <li id="demo" style="cursor: pointer; cursor: hand;padding: 1%;margin: 0%">
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="notifications.php">
                                <strong>Vedi tutte le notifiche</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw" style="color: #d9534f"></i> <i class="fa fa-caret-down" style="color: #d9534f"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                      <a>Loggato con: <?php echo "" . $_SESSION['username'] ; ?><a>
                        <li>
                            <a href="index.php"><i class="fa fa-home fa-fw"></i> Home</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-life-ring fa-fw"></i>Informazioni relative alla pubblicazione della tesi in AMS Laurea<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="https://studenti.unibo.it/sol/Manuale_di_laurea.pdf">Manuale tesi</a>
                                </li>
                                <li>
                                    <a href="http://www.sba.unibo.it/it/allegati/allegati-almadl/il-diritto-dautore-e-la-tesi-di-laurea/@@download/file/Tesi_diritti_autore.pdf">Linee guida</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="tables.html"><i class="fa fa-life-ring fa-fw"></i> Help desk Studenti Online<span class="fa arrow"></span></a>
							  <ul class="nav nav-second-level">
                                <li>
                                    <a>E-mail: E-mail servizio di supporto informatico</a>
                                </li>
								  <li>
                                    <a>Telefono: +39 051 20 99 882</a>
                                </li>
								  <li>
                                    <a>Orari: Lunedì - Venerdì 9:00-13:00 e 14:00-17:00</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i>Informazioni generali sui servizi agli studenti<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.html"></a>Per informazioni generali’</a>
                                </li>
                                <li>
                                    <a href="pagina.html">URP</a>
                                </li>
                                <li>
                                    <a href="pagina.html">A chi chiedere informazioni</a>
                                </li>
                                <li>
                                    <a href="pagina.html">Contatti per studenti internazionali</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Studenti Online</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-check-square-o fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Prove ammissione o verifica requisiti</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Iscriviti</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Immatricolazioni</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Iscriviti a un corso di studi</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-edit fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Esami-Alma Esami</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Prenota esami</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-phone fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Prenotazioni</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Prove, test, colloqui,..</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-thumb-tack fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Bandi</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Opportunità e offerte</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div><div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-eur fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Situazione tasse - Iscrizione </div>
                                </div>
                            </div>
                        </div>
                        <a href="situazione_tasse.php">
                            <div class="panel-footer">
                                <span class="pull-left">Situazione tasse</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-graduation-cap fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Laurea</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Procedimento di laurea</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-refresh fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Passaggio di corso</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Cambia corso</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-sign-out fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Trasferimento</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Richiedi trasferimento</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-times fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Rinuncia agli studi</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Compila procedura</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa glyphicon glyphicon-hourglass fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Sospensione degli studi</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Compila richiesta</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-globe fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Mobilità internazionale-AlmaRM</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Vedi Dettagli</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-hand-o-up fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Alma Orienta</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Prenota colloquio</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
	</div>
</div>


</body>

</html>
<?php
}
else {
	include("login.php");
}

?>
