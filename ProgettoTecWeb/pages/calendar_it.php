  
<?php
  /**
  * @author  Xu Ding
  * @email   thedilab@gmail.com
  * @website http://www.StarTutorial.com
  * @revised by Alessandro Marinuzzi
  * @website http://www.alecos.it/
  **/

  class Calendar {
    /**
    ** Constructor
    **/

    public function __construct($roww,$num_roww,$conn) {
      $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
      $this->row = $roww;
      $this->num_row = $num_roww;
      $this->connection = $conn;
    }
    /********************* PROPERTY ********************/
    private $dayLabels = array("Lun", "Mar", "Mer", "Gio", "Ven", "Sab", "Dom");
    private $currentYear = 0;
    private $currentMonth = 0;
    private $currentDay = 0;
    private $currentDate = null;
    private $daysInMonth = 0;
    private $naviHref = null;
    private $row;
    private $num_row;
    private $connection;

    /********************* PUBLIC **********************/
    /**
    ** print out the calendar
    **/
    public function show() {
      $year = null;
      $month = null;
      //Setto l'anno e il mese
      if (null == $year && isset($_GET['year'])) {
        $year = $_GET['year'];
      } elseif (null == $year) {
        $year = date("Y", time());
      }
      if (null == $month && isset($_GET['month'])) {
        $month = $_GET['month'];
      } elseif (null == $month) {
        $month = date("m", time());
      }
      $this->currentYear = $year;
      $this->currentMonth = $month;

      $this->daysInMonth = $this->_daysInMonth($month, $year);
      $content = '<div id="calendar">' . "\r\n" . '<div class="calendar_box">' . "\r\n" . $this->_createNavi() . "\r\n" . '</div>' . "\r\n" . '<div class="calendar_content">' . "\r\n" . '<ul class="calendar_label">' . "\r\n" . $this->_createLabels() . '</ul>' . "\r\n";
      $content .= '<div class="calendar_clear"></div>' . "\r\n";
      $content .= '<ul class="calendar_dates">' . "\r\n";
      $weeksInMonth = $this->_weeksInMonth($month, $year);
      // Create weeks in a month
      for ($i = 0; $i < $weeksInMonth; $i++) {
        //Create days in a week
        for ($j = 1; $j <= 7; $j++) {
          $content .= $this->_showDay($i * 7 + $j);
        }
      }
      $content .= '</ul>' . "\r\n";
      $content .= '<div class="calendar_clear"></div>' . "\r\n";
      $content .= '</div>' . "\r\n";
      $content .= '</div>' . "\r\n";
      //$var = $var . "value" operatore per concatenare

      return $content;
    }
    /********************* PRIVATE **********************/
    /**
    ** create the li element for ul
    **/
    private function _showDay($cellNumber) {
      if ($this->currentDay == 0) {
        $firstDayOfTheWeek = date('N', strtotime($this->currentYear . '-' . $this->currentMonth . '-01'));
        if (intval($cellNumber) == intval($firstDayOfTheWeek)) {
          $this->currentDay = 1;
        }
      }
      if (($this->currentDay != 0) && ($this->currentDay <= $this->daysInMonth)) {
        $this->currentDate = date('Y-m-d', strtotime($this->currentYear . '-' . $this->currentMonth . '-' . ($this->currentDay)));
        $cellContent = $this->currentDay;
        $this->currentDay++;
      } else {
        $this->currentDate = null;
        $cellContent = null;
      }
      $today_day = date("d");
      $today_mon = date("m");
      $today_yea = date("Y");
      $class_day = ($cellContent == $today_day && $this->currentMonth == $today_mon && $this->currentYear == $today_yea ? "calendar_today" : "calendar_days");
      $time = strtotime($this->currentYear .'/'.$this->currentMonth.'/'.$cellContent);
      $newformat = date('Y-m-d',$time);


      for( $j=0 ; $j<$this->num_row;$j++ ){

        $time2 = strtotime($this->row[$j]);
        $newformat2 = date('Y-m-d',$time2);
        $popupBoxOnePosition =  "'popupBoxOnePosition'";

        if($newformat2 == $newformat){

          $matricola = $_SESSION['matricola'];
          $sqll= "SELECT * FROM `mylogin`.`eventi_studente` WHERE matricola='$matricola' and data ='$newformat2'";
          $resultt = $this->connection->query($sqll);
          $roww=$resultt->fetch_assoc();

        ?>  <div id="popupBoxOnePosition">
              <div class="popupBoxWrapper">
                <div class="bordo">
                  <div class="popupBoxHeader">
                    <h3>Data:</h3>
                    <h3>Ora:</h3>
                    <h3>Luogo:</h3>
                    <h3>Descrizione:</h3>
                  </div>
                  <div class="popupBoxText">
                    <p><?php echo $roww['data'] ?></p>
                    <p><?php echo $roww['ora'] ?></p>
                    <p><?php echo $roww['luogo'] ?></p>
                    <p><?php echo $roww['descrizione'] ?></p>
                  </div>
                </div>
                <div class="popupBoxChiudi">
                  <a id="chiudi" href="javascript:void(0)" onclick="toggle_visibility('popupBoxOnePosition')">Chiudi</a>
                </div>
              </div>
            </div>
            <style type="text/css">

        			#popupBoxOnePosition{
        				top: 0; left: 0; position: fixed; width: 100%; height: 120%;
        				background-color: rgba(20,0,0,0.7);
                display: none;
                font-family: sans-serif;
        			}

              .popupBoxWrapper{
                background-color: #FFF;
                border-radius: 15px;
                border: 2px solid black;
                width: 35%;
                height: 40%;
                margin-top: 10%;
                margin-left: 30%;
              }

        			.popupBoxHeader{
                padding-left: 10%;
                width: 50%;
                height: 70%;
                float: left;
        			}

              .popupBoxText{
                font-size: 120%;
                padding-left: 8%;
                padding-top: 4%;
                width: 50%;
                height: 70%;
                float: right;
        			}

              .bordo{
                border: 2px solid lightgray;
                border-radius: 10px;
                width: 90%;
                height: 70%;
                margin: 5%;
              }

              .popupBoxChiudi{
                margin-top: 0%;
                padding-left: 42%;
                float: left;
              }

              #chiudi{

                padding: 2%;
                text-decoration: none;
                color: #d9534f;
                font-size: 120%;
                font-weight: bolder;

              }

              #chiudi:hover{
                border: 1px solid #d9534f;
                background-color: #d9534f;
                border-radius: 5px;
                padding: 2%;
                color: white;
                text-decoration: none;
                font-size: 120%;
                font-weight: bolder;
              }

              h3{
                margin-bottom: 10%;
              }

              p{
                margin-bottom: 13%;
              }

        		</style>
          <?php
           return '<li  class="' . $class_day . '_event"><a onclick="toggle_visibility('.$popupBoxOnePosition.')">' . $cellContent . '</a></li>' . "\r\n";
        }
      }
      return '<li class="' . $class_day . '">' . $cellContent . '</li>' . "\r\n";

    }

    /**
    ** Creo navigator per spostarmi da un mese all'altro
    **/
    private function _createNavi() {

      $nextMonth = $this->currentMonth == 12 ? 1 : intval($this->currentMonth)+1;
      $nextYear = $this->currentMonth == 12 ? intval($this->currentYear)+1 : $this->currentYear;
      $preMonth = $this->currentMonth == 1 ? 12 : intval($this->currentMonth)-1;
      $preYear = $this->currentMonth == 1 ? intval($this->currentYear)-1 : $this->currentYear;
      //strtotime
      $getMonth = date('Y M', strtotime($this->currentYear . '-' . $this->currentMonth . '-1'));

      $english = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
      $italian = array("Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dic");
      //str_ireplace This function returns a string or an array with all occurrences of search in subject (ignoring case) replaced with the given replace value.
      $thisMonth = str_ireplace($english, $italian, $getMonth);
      return '<div class="calendar_header">' . "\r\n" . '<a class="calendar_prev" href="' . $this->naviHref . '?month=' . sprintf('%02d', $preMonth) . '&amp;year=' . $preYear.'" >Precedente</a>' . "\r\n" . '<span class="calendar_title">' . $thisMonth . '</span>' . "\r\n" . '<a class="calendar_next" href="' . $this->naviHref . '?month=' . sprintf("%02d", $nextMonth) . '&amp;year=' . $nextYear . '">Prossimo</a>' . "\r\n"  . '</div>';
    }
    /**
    ** create calendar week labels
    **/
    private function _createLabels() {
      $content = '';
      foreach ($this->dayLabels as $index => $label) {
        $content .= '<li class="calendar_names">' . $label.'</li>' . "\r\n";
      }
      return $content;
    }
    /**
    ** calculate number of weeks in a particular month
    **/
    private function _weeksInMonth($month = null, $year = null) {
      if (null == ($year)) {
        $year = date("Y", time());
      }
      if (null == ($month)) {
        $month = date("m", time());
      }
      // trova il numero di giorni in questo mese per poterli suddividere in settimane
      $daysInMonths = $this->_daysInMonth($month, $year);
      $numOfweeks = ($daysInMonths % 7 == 0 ? 0 : 1) + intval($daysInMonths / 7);
      //date('N') rappresentazione numerica del giorno nella settimana
      $monthEndingDay = date('N',strtotime($year . '-' . $month . '-' . $daysInMonths));
      $monthStartDay = date('N',strtotime($year . '-' . $month . '-01'));
      if ($monthEndingDay < $monthStartDay) {
        $numOfweeks++;
      }
      return $numOfweeks;
    }
    /**
    ** calculate number of days in a particular month
    **/
    private function _daysInMonth($month = null, $year = null) {
      if (null == ($year)) $year = date("Y",time());
      if (null == ($month)) $month = date("m",time());
      return date('t', strtotime($year . '-' . $month . '-01'));
    }
  }

    require 'connect.php';
    $matricola = $_SESSION['matricola'];

    $sqll= "SELECT * FROM `mylogin`.`eventi_studente` WHERE matricola='$matricola'";
    $resultt = $connection->query($sqll);
    $num_row = $resultt->num_rows;
    $i=0;
    while($roww=$resultt->fetch_assoc()){


      $data_event[$i] = $roww['data'];
      $i++;

    }

    $calendar = new Calendar($data_event,$num_row,$connection);

    echo $calendar->show();

?>



		<script type="text/javascript">



			    function toggle_visibility(id) {

			       var e = document.getElementById(id);
			       if(e.style.display == 'block')
			          e.style.display = 'none';
			       else
			          e.style.display = 'block';
			    }
		</script>
